/**
 * Created by ChristianFernando on 6/14/2016.
 */
/**
 * Socket.io configuration
 */

'use strict';


var _ = require("lodash");


module.exports = function (io) {
    // socket.io (v1.x.x) is powered by debug.
    // In order to see all the debug output, set DEBUG (in server/config/local.env.js) to including the desired scope.
    //
    // ex: DEBUG: "http*,socket.io:socket"

    // We can authenticate socket.io users and access their token through socket.handshake.decoded_token
    //
    // 1. You will need to send the token in `client/components/socket/socket.service.js`
    //
    // 2. Require authentication here:
    // io.use(require('io-jwt').authorize({
    //   secret: config.secrets.session,
    //   handshake: true
    // }));
    require('../code').register(io);




    function onDisconnect(socket) {

        console.log('\x1b[1m\x1b[32m');
        console.log('El socket %s se ha desconectado.', socket.id);
        console.log('\x1b[0m');

    }

    function onConnect(socket) {

        var ipClient=socket.request.connection.remoteAddress.substring(socket.request.connection.remoteAddress.lastIndexOf(":")+1,socket.request.connection.remoteAddress.length);

        console.log('\x1b[1m\x1b[33m');
        console.log('Socket conectado desde %s el %s a las %s con el id %s',ipClient, socket.id);
        console.log('\x1b[0m');

    }
   /* function sendData(message) {
        io.sockets.emit('message', message);
    }*/

    io.on('connection', function (socket) {
        socket.connectedAt = new Date();

        // Call onDisconnect.
        socket.on('disconnect', function () {
            onDisconnect(socket);
        });


        // Call onConnect.
        onConnect(socket);
    });
};