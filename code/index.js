'use strict';


var express = require('express');
var router = express.Router();
var pubnub = require('./../config');
var debug = require('debug')('nodetest:server');
var io;

router.register = function (ioInstance) {
  io = ioInstance;
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/api/send-message', function(req, res, next) {

  /* ---------------------------------------------------------------------------
   Publish Messages
   --------------------------------------------------------------------------- */
  pubnub.publish({
    channel   : 'optimiza_chile​',
    message   : req.body,
    callback  : function(e) {
      console.log( "SUCCESS!", e );
      res.status(200).json({success:true, data: e});
    },
    error     : function(e) {
      console.log( "FAILED! RETRY PUBLISH!", e );
      res.status(400).json({success:true, data: e});
    }
  });


});



/* ---------------------------------------------------------------------------
 Listen for Messages
 --------------------------------------------------------------------------- */
pubnub.subscribe({
  channel  : "optimiza_chile​",
  message : function(message) {
    message.text = normalize(message.text);
    console.log('New message ' + message);
    debug('New message ' + message);
    if(io){
      io.sockets.emit('message', message);
    }
  }
});


var normalize = (function() {
  var from = "∙%$&∙&%%/&&((/%&/%&/%&/%&/ç",
      to   = "",
      mapping = {};

  for(var i = 0, j = from.length; i < j; i++ )
    mapping[ from.charAt( i ) ] = to.charAt( i );

  return function( str ) {
    var ret = [];
    for( var i = 0, j = str.length; i < j; i++ ) {
      var c = str.charAt( i );
      if( mapping.hasOwnProperty( str.charAt( i ) ) )
        ret.push( mapping[ c ] );
      else
        ret.push( c );
    }
    return ret.join( '' );
  }

})();


module.exports = router;
